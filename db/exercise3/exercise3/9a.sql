
SELECT  university.`university_name`
       ,SUM(amount) AS 'collected_fees' 
       ,semester_fee.paid_year
  FROM semester_fee
       ,university
       ,student
       ,college
 WHERE semester_fee.stud_id = student.id
   AND student.college_id = college.college_id
   AND college.univ_code = university.university_code
   AND paid_status = 'paid'
   AND paid_year = '2020'