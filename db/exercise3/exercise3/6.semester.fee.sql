      USE  university;
   SELECT  university.university_name
          ,student.id AS roll_number
          ,student.stud_name AS student_name
          ,student.gender
          ,student.dob
          ,student.address
          ,college.college_name AS college_name
          ,d.dept_name
          ,semester_fee.amount
          ,semester_fee.paid_year
          ,semester_fee.paid_status
	FROM   university u
          ,college c
          ,department d 
          ,college_department cd
          ,student s
          ,syllabus sy
          ,semester_fee sf
   WHERE college.univ_code = university.university_code 
     AND university.university_code = d.univ_code 
     AND college_department.college_id = college.college_id 
     AND college_department.udept_code = d.dept_code
     AND student.college_id = college.college_id
     AND student.cdep_id = college_department.cdept_id
     AND syllabus.cdept_id = college_department.cdept_id
     AND semester_fee.stud_id = student.id
     AND semester_fee.cdept_id = college_department.cdept_id
   ORDER BY roll_no ;