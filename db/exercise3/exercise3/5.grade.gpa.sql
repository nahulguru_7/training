	 SELECT  roll_no
			,stud_name
            ,gender
            ,email
            ,phone
		    ,address
            ,semester
            ,grade
            ,credits
            ,GPA
	   FROM university.student student
            LEFT JOIN university.semester_result semester_result
            ON student.id = semester_result.stud_id
            LEFT JOIN university.college college
            ON student.college_id = college.college_id
      ORDER BY college.college_id 
            AND semester;
   