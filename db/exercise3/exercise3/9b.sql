   USE `university`;
SELECT  university.`university_name`
       ,college.`college_name`
       ,semester_fee.`semester`
       ,SUM(amount) AS 'collected_fees' 
       ,semester_fee.paid_year
  FROM  semester_fee
       ,university
       ,student
       ,college
 WHERE semester_fee.stud_id = student.id
   AND student.college_id = college.college_id
   AND college.univ_code = university.university_code
   AND university_name = 'anna university'
   AND paid_year = '2020'
   AND semester = '8'
   AND paid_status = 'paid';
 
 
 
 SELECT  university.`university_name`
        ,college.`college_name`
        ,semester_fee.`semester`
        ,SUM(amount) AS 'uncollected_fees' 
        ,semester_fee.paid_year
   FROM  semester_fee
        ,university
        ,student
        ,college
  WHERE semester_fee.stud_id = student.id
    AND student.college_id = college.college_id
    AND college.univ_code = university.university_code
    AND university_name = 'anna university'
    AND semester = '8'
    AND paid_status = 'unpaid';

