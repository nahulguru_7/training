     SELECT  name as employee_name
            ,dob
            ,mail
            ,phone
            ,college_name
            ,city
            ,dept_code
            ,dept_name
            ,desgn_name
            ,desgn_rank
	   FROM university.employee employee
            LEFT JOIN university.college college
            ON employee.college_id = college.college_id
            LEFT JOIN university.college_department
            ON employee.cdept_id = college_department.cdept_id
            LEFT JOIN university.department department
            ON college_department.udept_code = department.dept_code
            LEFT JOIN university.designation designation
            ON employee.desgn_id = designation.desgn_id
      WHERE college.univ_code = 'A001'
      ORDER BY desgn_rank
        AND college_name;