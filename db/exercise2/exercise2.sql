CREATE DATABASE office;
USE office;
CREATE TABLE department(
  dept_id INT NOT NULL AUTO_INCREMENT
  ,PRIMARY KEY(dept_id)
  ,name VARCHAR(50)
  );
  
  CREATE TABLE employee(
    emp_id INT NOT NULL AUTO_INCREMENT
    ,PRIMARY KEY(emp_id)
    ,first_name VARCHAR(50) NOT NULL
    ,surname VARCHAR(50)
    ,dob DATE NOT NULL
    ,date_of_joining DATE NOT NULL
    ,annual_salary FLOAT NOT NULL
    ,dept INT NOT NULL
    ,FOREIGN KEY (dept) REFERENCES department(dept_id)
  );
/*table created */ 
  INSERT INTO department(name)
    VALUES ("ITDesk")
		  ,("Finance")
          ,("Engineering")
          ,("HR")
          ,("Recruitment")
          ,("Facility");	
SELECT dept_id, name FROM department;
  INSERT INTO employee (first_name, surname, dob, date_of_joining, annual_salary, dept)
    VALUES ('SAI', 'SAKTHI', '1990-04-22', '2010-06-08', 4500000, 1)
          ,('MELVIN', 'VICTOR', '1988-06-18', '2007-07-21', 3000000,1)
          ,('NAVIN', 'KARTHIK', '1996-05-25', '2012-09-13', 5000000, 1)
          ,('KISHORE', 'KUMAR', '1994-07-18', '2011-10-06', 3500000, 1)
          ,('AMIR', 'NAWSAL', '1990-05-09', '2002-03-02', 2000000, 1)
          ,('RAJ', 'KUMAR', '1988-04-15', '2008-07-29', 600000, 2)
          ,('SANTHOSH', 'KAVIN', '1992-10-06', '2014-09-07', 250000, 2)
          ,('JABEZ', 'STONET', '1994-11-22', '2007-06-14', 5500000, 2)
          ,('AKSHAY', 'KUMAR', '1983-02-10', '1997-07-12', 3000000, 2)
          ,('GEORGINA', 'RODRIGUEZ', '1980-12-13', '1993-05-08', 2000000, 2)
          ,('LIONEL', 'MESSI', '1986-10-17', '2003-09-21', 6500000, 3)
          ,('CRISTIANO', 'RONALDO', '1984-01-01', '2000-01-20', 7500000, 3)
          ,('KYLIAN', 'MBAPPE', '1992-08-22', '2013-01-08', 4500000, 3)
          ,('JADEN', 'SANCHO', '1997-02-13', '2016-11-17', 450000, 3)
          ,('PAULO', 'DYBALA', '1989-12-26', '2009-05-11', 450000, 3)
          ,('PAUL', 'POGBA', '1988-05-17', '2006-10-24', 8000000, 4)
          ,('MARCUS', 'RASHFORD', '1990-06-04', '2011-11-11', 4500000, 4)
          ,('MASON', 'GREENWOOD', '1987-12-19', '2000-03-23', 400000, 4)
          ,('TAMMY', 'ABRAHAM', '1989-10-23', '2010-03-29', 800000, 4)
          ,('GARETH', 'BALE', '1997-01-01', '2018-12-30', 700000, 4)
          ,('HARRY', 'KANE', '1987-11-30', '2006-09-24', 700000, 5)
          ,('KEERTHI', 'KAPOOR', '1991-05-19', '2015-10-29', 600000, 5)
          ,('ERIC', 'DIER', '1993-05-29', '2016-12-06', 800000, 5)
          ,('GARRY', 'CAHILL', '1992-03-10', '2012-07-07', 600000, 5)
          ,('DANIEL', 'JAMES', '1987-04-14', '2010-11-09', 800000, 5)
          ,('LUKE', 'SHAW', '1989-08-12', '2007-10-18', 650000, 6)
          ,('MARCO', 'REUS', '1986-02-27', '2009-07-27', 9000000, 6)
          ,('JORDAN', 'HENDERSON', '1991-10-25', '2015-11-08', 600000, 6)
          ,('JAMIE', 'VARDY', '1990-08-27', '2007-06-21', 300000, 6)
          ,('JONNY', 'EVANS', '1981-03-20', '2007-07-07', 650000, 6);
  /*records inserted*/
SELECT emp_id, first_name, dob, date_of_joining, annual_salary, dept FROM employee;
UPDATE employee
  SET annual_salary = annual_salary + (annual_salary * (10/100))
    WHERE dept IN (SELECT dept_id FROM department);
/*10% incremented*/
SELECT emp_id, first_name, dob, date_of_joining, annual_salary, dept FROM employee;
SELECT emp_id, first_name, MAX(annual_salary) FROM employee
  WHERE dept = 1;
SELECT emp_id, first_name, MIN(annual_salary) FROM employee
  WHERE dept = 1;
SELECT emp_id, first_name, MAX(annual_salary) FROM employee
  WHERE dept = 2;
SELECT emp_id, first_name, MIN(annual_salary) FROM employee
  WHERE dept = 2;
SELECT emp_id, first_name, MAX(annual_salary) FROM employee
  WHERE dept = 3;
SELECT emp_id, first_name, MIN(annual_salary) FROM employee
  WHERE dept = 3;
SELECT emp_id, first_name, MAX(annual_salary) FROM employee
  WHERE dept = 4;
SELECT emp_id, first_name, MIN(annual_salary) FROM employee
  WHERE dept = 4;
SELECT emp_id, first_name, MAX(annual_salary) FROM employee
  WHERE dept = 5;
SELECT emp_id, first_name, MIN(annual_salary) FROM employee
  WHERE dept = 5;
SELECT emp_id, first_name, MAX(annual_salary) FROM employee
  WHERE dept = 6;
SELECT emp_id, first_name, MIN(annual_salary) FROM employee
  WHERE dept = 6;
SELECT AVG(annual_salary) FROM employee;
/*max,min,avg salaries calculated*/
SELECT emp.emp_id, emp.first_name, emp.annual_salary, dept.name
  FROM employee emp, department dept
    WHERE emp.dept = dept.dept_id AND emp.dept = 1;
SELECT emp_id, first_name, dob FROM employee WHERE dob = date(now());
SELECT emp_id, first_name, dob FROM employee
  WHERE dept IS NULL;
/*fresher*/
SELECT emp.emp_id, emp.first_name, dept.name 
  FROM employee emp, department dept
    WHERE emp.dept = dept.dept_id;
/*emplyoee details is fetched*/