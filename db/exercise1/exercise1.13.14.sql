DROP DATABASE store;
CREATE DATABASE store;
USE store;
CREATE TABLE stack(
id int NOT NULL AUTO_INCREMENT
,PRIMARY KEY(id)
,name varchar(50) NOT NULL
,price int NOT NULL
);
CREATE TABLE new_stack(
id int NOT NULL AUTO_INCREMENT
,PRIMARY KEY(id)
,name varchar(50) NOT NULL
,price int NOT NULL
);
INSERT INTO stack(name,price)
  VALUES('Carrot','60')
       ,('Potato','70')
       ,('Tomato','80');
INSERT INTO new_stack(name,price)
  VALUES('Ladies Finger','80')
       ,('Spinach','30')
       ,('Cauli Flower','50');
SELECT name,price
  FROM stack
    UNION ALL
      SELECT name,price
        FROM new_stack;
SELECT name,price
  FROM stack
    UNION distinct
      SELECT name,price
        FROM new_stack;
CREATE VIEW goods AS
SELECT id, name
FROM stack
WHERE price=80;