DROP DATABASE bookmyshow;
CREATE DATABASE bookmyshow;
/*database created*/
USE bookmyshow;
CREATE TABLE user(
id int NOT NULL AUTO_INCREMENT
,PRIMARY KEY(id)
,username varchar(50) NOT NULL
,age int NOT NULL
);
/*table created*/
RENAME TABLE user to Costumer;
/*table renamed*/
DROP TABLE costumer;
/*table droped*/
CREATE TABLE user(
id int NOT NULL AUTO_INCREMENT
,PRIMARY KEY(id)
,username varchar(50) NOT NULL
,age int NOT NULL
);
ALTER TABLE user
   ADD Address int NOT NULL;
/*column created*/
ALTER TABLE user
  MODIFY Address varchar(50) NOT NULL;
/*column modifies*/
ALTER TABLE user
  CHANGE COLUMN Address  addr varchar(50) NOT NULL;
/*column renamed*/
ALTER TABLE user
  DROP COLUMN addr;
/*column dropped*/
CREATE TABLE movie(
id int NOT NULL AUTO_INCREMENT
,PRIMARY KEY(id)
,name varchar(50) NOT NULL
,genre varchar(50) NOT NULL
);
CREATE TABLE theatre(
id int NOT NULL AUTO_INCREMENT
,PRIMARY KEY(id)
,name varchar(50) NOT NULL
,seat_no int NOT NULL
,Amount float NOT NULL
);
CREATE TABLE user_data(
id int NOT NULL AUTO_INCREMENT
,PRIMARY KEY(id)
,movie_id int NOT NULL
,FOREIGN KEY(movie_id) REFERENCES movie(id)
,theatre_id int NOT NULL
,FOREIGN KEY(theatre_id) REFERENCES theatre(id)
);
/*constrains are used*/
DROP TABLE  user_data;
INSERT INTO user(username,age)
  VALUES('RAJA','23');
SHOW COLUMNS FROM user;
TRUNCATE TABLE user;
/*table truncated*/
SELECT id,username,age FROM user;
INSERT INTO user(username,age)
  VALUES('NAHUL','20') 
	   ,('BEN','24')
       ,('TONY','18')
       ,('GLENN','22')
       ,('CHRIS','26')
       ,('LIAM','28');
/*records inserted*/
INSERT INTO theatre(name,seat_no,amount)
  VALUES('A','24','300')
	   ,('B','25','400')
       ,('C','26','150')
       ,('D','27','100')
       ,('E','29','250')
       ,('F','30','260');
UPDATE theatre
  SET amount = 200
    WHERE id = 26;
/*field updated*/
DELETE FROM theatre
  WHERE id = 26;
  /*field deleted*/
SELECT * FROM user;
  /*all data fetched*/
SELECT id,username,age FROM user
    WHERE id = 2;
SELECT id,name,seat_no,amount FROM theatre
  WHERE amount = '200' OR amount = '300';
SELECT * FROM theatre
  WHERE amount = '200' AND amount = '150';
SELECT * FROM theatre
  WHERE NOT amount = '400';
/*operators used*/
SELECT MAX(amount) FROM theatre;
/*max function*/
SELECT MIN(amount) FROM theatre;
/*min function*/
SELECT AVG(amount) FROM theatre;
/*avg function*/
SELECT COUNT(distinct(id)) FROM theatre;
/*count function*/
SELECT SUM(amount) FROM theatre;
/*sum function*/
SELECT id,name FROM theatre
  ORDER BY name ASC limit 2;
/*first function*/
SELECT id,name FROM theatre
  ORDER BY name DESC limit 3;
/*last function*/
CREATE TABLE user_data(
id int NOT NULL AUTO_INCREMENT
,PRIMARY KEY(id)
,movie_id int NOT NULL
,FOREIGN KEY(movie_id) REFERENCES movie(id)
,theatre_id int NOT NULL
,FOREIGN KEY(theatre_id) REFERENCES theatre(id)
);
SELECT theatre.id,theatre.name FROM theatre theatre
  WHERE theatre.id IN(
    SELECT usr.theatre_id FROM user_data usr
    );
SELECT usr.id,theatre.id,theatre.name,mov.id
,mov.name,theatre.amount
  FROM user_data usr
    INNER JOIN theatre theatre
      ON usr.theatre_id = theatre.id
        INNER JOIN movie mov 
          ON usr.movie_id = mov.id;
/*inner*/
SELECT usr.id,theatre.id,theatre.name,mov.id
,mov.name,theatre.amount
  FROM user_data usr
    RIGHT JOIN theatre theatre
      ON usr.theatre_id = theatre.id
        RIGHT JOIN movie mov 
          ON usr.movie_id = mov.id;
/*right*/
SELECT usr.id,theatre.id,theatre.name,mov.id
,mov.name,theatre.amount
  FROM user_data usr
    LEFT JOIN theatre theatre
      ON usr.theatre_id = theatre.id
        LEFT JOIN movie mov 
          ON usr.movie_id = mov.id;
/*left*/
SELECT usr.id,theatre.id,theatre.name,mov.id
,mov.name,theatre.amount
  FROM user_data usr
    CROSS JOIN theatre theatre
      ON usr.theatre_id = theatre.id
        CROSS JOIN movie mov 
          ON usr.movie_id = mov.id;
/*cross*/