package com.training.java.core.abstractdemo;
/*
 Problem Statement
 =================
  Demonstrate abstract classes using Shape class.
    - Shape class should have methods to calculate area and perimeter
    - Define two more class Circle and Square by extending Shape class and implement the calculation for each class respectively.
  ----------------------------------------Word Breakdown Structure(WBS)--------------------------------------------------------
1.Requirements
   - Demonstrate abstract classes using Shape class.
   - Shape class should have methods to calculate area and perimeter
2.Entities
    - Shape (Abstract class)
    - Square 
    - Circle
3.Function Declaration
    - Square() (Constructor)
    - Circle() (Constructor)
    - showArea()
    - showPerimeter()
    - public static void main(String args[])
4.Jobs to be done:
    1.Create a scanner object to get input from the user.
    2.Get input to Switch between Square and Circle class 
    3.Invoke the Square and Circle class methods and  that methods declared in abstract class 
            3.1)Returns Square formula Area and Perimeter results.
            3.2)Returns Circle formula Area and Perimeter results.
    4.Prints the returned results.
*/


import java.util.Scanner;
abstract class Shape {
    abstract public double showArea();
    abstract public double showPerimeter();
}
class Square extends Shape {
    private double side;
    public Square(double side) {
        this.side= side;
    }
    public double showArea() {
        return side*side;
    }
    public double showPerimeter() {
        return 4*side;
    }
}

class Circle extends Shape {
    private double radius;
    public Circle(double radius) {
        this.radius = radius;
    }
    public double showArea() {
        return (3.14f*(radius*radius));
    }
    public double showPerimeter() {
        return 2*3.14f*radius;
    }
}

public class AbstractDemo {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);   
        System.out.println("Choose the Shape");
        System.out.println("1. Square 2.Circle");
        int number = scanner.nextInt();
        switch(number) {
            case 1:
                System.out.println("Enter the length");
                int side = scanner.nextInt();
                Square square = new Square(side);
                System.out.println("The Area of the Square is "+ square.showArea());
                System.out.println("The Perimeter of the Square is "+ square.showPerimeter());
                break;
            case 2:
                System.out.println("Enter the radius");
                int radius = scanner.nextInt(); 
                Circle circle = new Circle(radius);          
                System.out.println("The Area of the Circle is "+ circle.showArea());
                System.out.println("ThePerimeter of the Circle is "+ circle.showPerimeter());
                break;
        }    
    }
}

