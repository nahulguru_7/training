package com.training.java.core.misc;

// What is the output from the following code?

/* ---------Word Breakdown Structure(WBS)--------
1.Requirments
    -Output of the following code.
2.Entities
    -IdentifyMyParts
3.Function Declaration
    -public static void main(String args[])
4.Jobs to be done
    1.Create an class called IdentifyVariables.
    2.Inside the class declaring two variables class variable x and instance variable y.
    3.Declare main method inside the class.
    4.In the main method create two object and declare two object with each variable
    5.Initializing values using two object with each variables and
    6.Print the initialized values using object and one variable instance so using class name 
print the value.*/

public class IdentifyMyParts {
    public static int x = 7;
    public int y = 3;
    public static void main(String args[]) {
        IdentifyMyParts a = new IdentifyMyParts();
        IdentifyMyParts b = new IdentifyMyParts();
        a.y = 5;
        b.y = 6;
        a.x = 1;
        b.x = 2;
        System.out.println("a.y = " + a.y);
        System.out.println("b.y = " + b.y);
        System.out.println("a.x = " + a.x);
        System.out.println("b.x = " + b.x);
        System.out.println("IdentifyMyParts.x = " + IdentifyMyParts.x);
        }
    }
 
// What is the output from the following code?
/*Output
a.y = 5
b.y = 6
a.x = 2
b.x = 2
IdentifyMyParts.x = 2*/