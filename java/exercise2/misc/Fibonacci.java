package com.training.java.core.misc;

/*print fibinocci using for loop, while loop and recursion

-------------Word Breakdown Structure(WBS)------------------
1.Requirments
    -Print fibinocci using for loop, while loop and recursion.
2.Entities
    -Fibonacci
3.Function Declaration
    -public Fibonacci(int count, int number1,int number2)
    -public void FibonacciFor(int count, int number1,int number2)
    -public void FibonacciWhile(int count, int number1,int number2)
    -public void FibonacciRecursion(int count, int number1,int number2)
    -public static void main(String[] args)
4.Jobs to be done
    1.Create a class called Fibonacci.
    2.Declare an three integer variable and class constructor to initialise the value and declare
three methods FibonacciFor, FibonacciWhile and FibonacciWhile passing three integer parameter.
    3.Declare the main method and in the main method create an object and passing arguments and 
using object invoke the FibonacciFor, FibonacciWhile and FibonacciRecursion methods.
    4.In the FibonacciFor method uisng for loop initialise the value, check the variable less than 
or equal to count variable and increment the initialised variable.Print the first value and adding 
number1 and number2 initialised to number3 then swapping three variables.
    5.In the FibonacciWhile method using while loop initialise the value, in while condition check 
the variable less than or equal to count variable.In loop print number1 variable then adding number1
and number2 initialised to number3 and then swapping the three variables.To change the variable
increment the initialised variable.
    6.In the FibonacciRecursion check count greater than to zero and declare number3 initialise 
number1 and number2 added value and swap the three variable values to recursion using invoke same 
FibonacciRecursion method in parameter decrementing the count value.
    7.In the main method create an object and invoke the three methods FibonacciFor, FibonacciWhile
and FibonacciRecursion with passing three variable.*/

public class Fibonacci {
    int count;
    int number1;
    int number2;
    
    public Fibonacci(int count, int number1,int number2) {
        this.count = count;
        this.number1 = number1;
        this.number2 = number2;
    }
    public void FibonacciFor(int count, int number1,int number2) {
        for (int i = 1; i <= count; i++)
        {
            System.out.print(" " + number1 + " ");
            int number3 = number1 + number2;
            number1 = number2;
            number2 = number3;
        }
    }
    public void FibonacciWhile(int count, int number1,int number2) {
        int i = 1;
        while(i <= count)
        {
            System.out.print(" "+number1+" ");
            int number3 = number1 + number2;
            number1 = number2;
            number2 = number3;
            i++;
        }
    }
    public void FibonacciRecursion(int count, int number1,int number2) { {
        if(count > 0) {
            System.out.print(" "+ number1 +" ");
            int number3 = number1 + number2;
            number1 = number2;
            number2 = number3;
            FibonacciRecursion(count - 1,number1,number2);
        }
    }
}

    public static void main(String[] args) {
        Fibonacci fibonacci = new Fibonacci(9,0,1);
        System.out.print("Fibonacci Series using \nFor loop");
        fibonacci.FibonacciFor(9,0,1);
        System.out.print("\nWhile loop");
        fibonacci.FibonacciWhile(9,0,1);
        System.out.print("\nRecursion");
        fibonacci.FibonacciRecursion(9,0,1);
    }
}