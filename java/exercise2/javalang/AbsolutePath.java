package com.training.java.core.javalang;
/*
  Print the absolute path of the .class file of the current class
Open the source file of the running class in Notepad++ - do NOT hardcode/specify the source file 
in the code; find the source file using APIs
-------------Word Breakdown Structure(WBS)------------------
1.Requirments
    -Print the absolute path of the .class file of the current class.
2.Entities
    -AbsolutePath
3.Function Declaration
    -public AbsolutePath()
    -public static void main( String[] args )
4.Jobs to be done
    1.Create AbsolutePath class object and process the constructor for AbsolutePath class
           1.1)Invoke getClass method, getClassLoader method in initalise path ClassLoader
           1.2).Invoke getResource method to find class path file to current URL variable.
           1.3)Print the Path of a File without HardCoding using getProperty() method parameter user directory.
    */

import java.net.URL;

class AbsolutePath {
   public AbsolutePath() {
      ClassLoader path = this.getClass().getClassLoader();
      URL current = path.getResource("AbsolutePath.class");
      System.err.println("Url=" + current);
      //Getting the Path of a File without HardCoding//
        System.out.println(System.getProperty("user.dir"));
   }
   public static void main( String[] args ) {
      AbsolutePath path = new AbsolutePath();
   }
}