package com.training.java.core.javadoc;
/*
 
 What Double method can you use to detect whether a floating-point number has the special value Not a Number (NaN)?
      Answer: isNaN
*/