package com.training.java.core.datatype;


/*
 Problem Statement
 =================
 Print the type of the result value of following expressions
    - 100 / 24
    - 100.10 / 10
    - 'Z' / 2
    - 10.5 / 0.5
    - 12.4 % 5.5
    - 100 % 56

-------------Word Breakdown Structure(WBS)------------------

Requirement:
    -Print the type of the result value of following expressions
Entity:
    -ExpressionType
Function Declaration:
    -public static void main(String[] args)
Jobs to be done:
    1.Declare a,b,c variable in integer and double datatype.
    2.Calculate the given operation
          2.1)Prints some of operations of results using getClass method to gets the class of the object 
          2.2)And getName() method to returns only the name of the class.
*/


public class ExpressionType {
    public static void main(String[] args) {
        Integer a = 100;
        Double b = 10.5;
        Double c = 12.4;
        Integer sm1 = a/24;
        Double sm2 = b/ 0.5;
        Double sm3 = c% 5.5;
        Integer sm4 = a % 56;
        System.out.println(sm1.getClass().getName() +sm1);
        System.out.println(100.10 / 10);
        System.out.println('Z' / 2);
        System.out.println(sm2.getClass().getName() +sm2);
        System.out.println(sm3.getClass().getName() +sm3);
        System.out.println(sm4.getClass().getName() + sm4);
        System.out.println('Z'*2);
    }
}