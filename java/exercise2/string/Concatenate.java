package com.training.java.core.string;
/*
Show two ways to concatenate the following two strings together to get the string "Hi, mom.":
    String hi = "Hi, ";
    String mom = "mom.";
    
  Answer:  hi.concat(mom) and hi + mom.
  ''''''     
        
*/