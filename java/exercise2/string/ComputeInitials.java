package com.training.java.core.string;

/*Write a program that computes your initials from your full name and displays them.
-------------Word Breakdown Structure(WBS)-----------------
1.Requirments
    program that computes your initials from your full name and displays them.
2.Entities
    ComputeInitials
3.Function Declaration
    public class ComputeInitials()
4.Jobs to be done
    1.Declare the String variable and initialize value as myName.
    2.Create myInitials an object using StringBuffer class and using object find length of the given name. 
    3.Using for loop initialize value , check the length of name and increment variable
and inside the loop check index is uppercase if it is uppercase append to created string object
    4.Print the name of initialise in the loop end.*/
    
    
public class ComputeInitials {
    public static void main(String[] args) {
        String myName = "Santheesh Arumugam";
        StringBuffer myInitials = new StringBuffer();
        int length = myName.length();

        for (int i = 0; i < length; i++) {
            if (Character.isUpperCase(myName.charAt(i))) {
                myInitials.append(myName.charAt(i));
            }
        }
        System.out.println("My initials are: " + myInitials);
    }
}


//----------Output--------
// My initials are: SA