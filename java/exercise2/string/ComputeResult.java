package com.training.java.core.string;
//In the following program, called ComputeResult, what is the value of word after each numbered line executes?
    public class ComputeResult {
        public static void main(String[] args) {
            String string = "software";
            StringBuilder word = new StringBuilder("hi");
            int index = string.indexOf('a');

    /*1*/   word.setCharAt(0, string.charAt(0));
    /*2*/   word.setCharAt(1, string.charAt(string.length()-1));
    /*3*/   word.insert(1, string.charAt(4));
    /*4*/   word.append(string.substring(1,4));
    /*5*/   word.insert(3, (string.substring(index, index+2) + " "));

            System.out.println(word);
        }
    }
/*Word Breakdown Structure(WBS)
1.Requirments
    -what is the value of word after each numbered line executes
2.Entities
    -ComputeResult
3.Function Declaration
    -public class ComputeResult()
    -public static void main(String[] args)
4.Jobs to be done
    1.Declare the string variable and initialize value.
    2.Creating word object using StringBuilder and passing a string.
    3.Printing the string index and using object selected values.*/
    

/*Answer:

si
se
swe
sweoft
swear oft


------Output--------
  swear oft  */