package com.training.java.core.operators;
//In the following program, explain why the value "6" is printed twice in a row:

/*---------Word Breakdown Structure(WBS)-------------
1.Requirments
    -Explain why the value "6" is printed twice in a row.
2.Entities
    -PrePostDemo
3.Function Declaration
    -public static void main(String[] args)
4.Jobs to be done
    1.Declare integer variable number with initializing a value.
    2.Increments the number before the current expression is evaluted and prints the value.
    3.Increments the number after the current expression is evaluted and prints the value.
    4.Increments the number before the current expression is evaluted in the println() method.
    5.Again increments the number after the current expression is evaluted in the println method
    6.Prints the value.*/
    
class PrePostDemo {
    public static void main(String[] args) {
        int number = 3;
        number++;
        System.out.println(number);    // "4"
        ++number;
        System.out.println(number);    // "5"
        System.out.println(++number);  // "6"
        System.out.println(number++);  // "6"
        System.out.println(number);    // "7"
    }
}

/*Output
4
5
6 
6 
7*/

/* why the value "6" is printed twice in a row?
     When using an increment the number before the current expression is evaluted.Its prints the
incremented value but using an increment the number after the current expression is evaluted.Its 
first prints the value and then it increment the value.So, the value "6" is printed twice in a row.*/