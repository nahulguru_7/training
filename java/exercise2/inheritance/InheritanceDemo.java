package com.training.java.core.inheritance;
/*Demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of objects

Word Breakdown Structure(WBS)
1.Requirement:
    -Demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake 
class of objects.

2.Entity:
    -Animal
    -Dog
    -Cat
    -Snake

3.Function Declaration:
    -public void eatfood()
    -public void eatfood(int kg)

4.Jobs to be done:
  1.Invoke subclass Snake.
         1.1)eatfood() method.
         1.2)eatfood() method with parameters.
  2.Invoke superclass Animal.
         2.1)eatfood() method.
  3.Invoke subclass Cat.
         3.1)eatfood() method.
  4.
 */


class Animal {
    public void eatfood() {
        System.out.println("Eating food..");
    }
    public void eatfood(int kg) {
        System.out.println(String.format("Eating more food..",kg));
    }
}
class Dog extends Animal {
    public void eatfood() {
        System.out.println("Eating pedigree..");
    }
    public void eatfood(int kg) {
        System.out.println(String.format("Eating %dkg of pedigree..",kg));
    }
}
class Cat extends Animal {
    public void eatfood() {
        System.out.println("Eating a fish..");
    }
    public void eatfood(int kg) {
        System.out.println(String.format("Eating %dkg of fish..",kg));
    }
}
class Snake extends Animal {
    public void eatfood() {
        System.out.println("Eating a frog..");
    }
    public void eatfood(int kg) {
        System.out.println(String.format("Eating %d frogs..",kg));
    }
}
public class InheritanceDemo {
    public static void main(String[] args) {
        //method overloading
        Snake cobra = new Snake();
        cobra.eatfood();
        cobra.eatfood(4);
        //method overriding
        Animal cat = new Animal();
        cat.eatfood();
        Cat ragdoll = new Cat();
        ragdoll.eatfood();
    }
}