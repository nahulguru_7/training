package com.training.java.core.enumeration;
//compare the enum values using equal method and == operator

/*----Word Breakdown Structure(WBS)----
1.Requirments
    -Compare the enum values using equal method and == operator
2.Entities
    Enumvalues
3.Function Declaration
    -public static void main(String[] args) 
4.Jobs to be done
    1.Declare variable as Day initailse null value.
    2.Compare two enum and one enum with null value using 
         2.1)equal == operator
    3.Compare two enum and one enum with null value using
        3.1)equal() method
     */

class Enumvalues { 
    public enum Day { 
                    MON, 
                    TUE, 
                    WED, 
                    THU, 
                    FRI, 
                    SAT, 
                    SUN 
          } 
    public enum Month { 
                    JAN, 
                    FEB, 
                    MAR, 
                    APR, 
                    MAY, 
                    JUN, 
                    JULY } 
    public static void main(String[] args) 
    {
        Day d = null;
        // Comparing two enum members which are from different enum type
        // using == operator
        //System.out.println(Month.JAN == Day.MON);
        System.out.println(d == Day.MON);
        //When checking using initialized null variable to enum values using operator gives false.

        // Comparing two enum members which are from different enum type 
        // using .equals() method
        System.out.println(d.equals(Day.MON)); 
        System.out.println(Month.JAN.equals(Day.MON)); 
        /*When checking using initialized null variable to enum values using equals method 
        gives false. */
    }
}
/* Using == operator and equals method gives when one enum value is null 
--------Output--------
false 
Exception in thread "main" java.lang.NullPointerException

But Comparing two enum values using == operator and equal() method
error: incomparable types: Month and Day*/

