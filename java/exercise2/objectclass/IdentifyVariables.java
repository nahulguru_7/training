package com.training.java.core.objectclass;
//What are the class variables?
//What are the instance variables?


//Consider the following class:
    public class IdentifyVariables {
        public static int x = 7;
        public int y = 3;
    }

/*What are the class variables?
       Class variables are shared between all objects. Changing the variable’s data through one 
object will change the data for all the objects.In the above code x is class variable because
before declaring the variable adding static keyword called as class variable.

What are the instance variables?
       An instance variable is individually created for a single object of a class.In the above code
y is instance variable.*/