package com.training.java.core.objectclass;
// What is the output from the following code?

/* ---------Word Breakdown Structure(WBS)--------
1.Requirments
    - Output of the following code.
2.Entities
    - IdentifyMyParts
3.Function Declaration
    - public static void main(String args[])
4.Jobs to be done
    1.Declare two variables class variable x and instance variable y.
    2.Declare two object with each variable and initializing values using two object with each variables.
    3.Initialised values using object and each variable instance using class name 
    4.Print the value.

*/

public class IdentifyMyParts {
    public static int x = 7;
    public int y = 3;
    public static void main(String args[]) {
        IdentifyMyParts a = new IdentifyMyParts();
        IdentifyMyParts b = new IdentifyMyParts();
        a.y = 5;
        b.y = 6;
        a.x = 1;
        b.x = 2;
        System.out.println("a.y = " + a.y);
        System.out.println("b.y = " + b.y);
        System.out.println("a.x = " + a.x);
        System.out.println("b.x = " + b.x);
        System.out.println("IdentifyMyParts.x = " + IdentifyMyParts.x);
        }
    }
 
// What is the output from the following code?
/*Output
a.y = 5
b.y = 6
a.x = 2
b.x = 2
IdentifyMyParts.x = 2*/