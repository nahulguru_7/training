package com.training.java.core.objectclass;
/*Given the following class, called NumberHolder,
    write some code that creates an instance of the class and initializes its two member variables with provided values,
    and then displays the value of each member variable.
----------Word Breakdown Structure(WBS)--------
1.Requirments
    Creates an instance of the class and initializes its two member variables with provided values,
and then displays the value of each member variable.
2.Entities
    -NumberHolder
3.Function Declaration
    -public void display()
    -public static void main(String args[])
4.Jobs to be done
    1.Declare two instance variable inside the class.
    2.Invoke the NumberHolder class display method with passing two arguments.
         2.1)Prints the int and float values
             */


public class NumberHolder {
    public int anInt;
    public float aFloat;
    public void display(int anInt,float aFloat) {
        System.out.println("anInt = " + anInt +" "+ "aFloat = " + aFloat);
    }
    public static void main(String args[]) {
        NumberHolder numHold = new NumberHolder();
        numHold.anInt = 25;
        numHold.aFloat = 7.5f;
        numHold.display(numHold.anInt,numHold.aFloat);
    }
}

/*Output
anInt = 25 aFloat = 7.5*/