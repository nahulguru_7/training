/*THROWS
  ----------------------------WBS--------------------------------
 1.Requirements:
   -Throws with example. 
 2.Entity:
   - ThrowsDemo
 3.Function Declaration:
   - public static void main(String[] args)
 4.Jobs to be done:  
   1.Create ThrowsDemo class object.
   2.Invoke ThrowsDemo class division method with passing two arguments.
      2.1)Check ArithmeticException error.
      2.2)Divide a by b and return value.

Psudeo Code:
''''''''''''
public class ThrowsDemo{  
   int division(int a, int b) throws ArithmeticException{  
	int t = a/b;
	return t;
   }  
   public static void main(String[] args){  
	   ThrowsDemo obj = new ThrowsDemo();
	try{
	   System.out.println(obj.division(15,0));  
	}
	catch(ArithmeticException e){
	   System.out.println("You shouldn't divide number by zero");
	}
   }  
}

----------------------------Program------------------------------*/
public class ThrowsDemo{  
   int division(int a, int b) throws ArithmeticException{  
	int t = a/b;
	return t;
   }  
   public static void main(String[] args){  
	   ThrowsDemo obj = new ThrowsDemo();
	try{
	   System.out.println(obj.division(15,0));  
	}
	catch(ArithmeticException e){
	   System.out.println("You shouldn't divide number by zero");
	}
   }  
}
