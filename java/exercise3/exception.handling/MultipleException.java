/*Demonstrate the catching multiple exception with example.
-----------------------------------------WBS--------------------------------------- 
 Requirements:
   - Demonstrate the catching multiple exception with example. 
 Entity:
   - MultipleException
 Function Declaration:
   - public static void main(String[] args)
 Jobs to be done:
   1.Try block declare and Initialize the array value.
       1.1)Array 4th index initialise number divided by zero.
   2.Catch blocks catches specified error
       2.1)Print the error using getMessage method 
       
Psudeo Code:
'''''''''''
public class MultipleException {    
    public static void main(String[] args) {    
        try {    
            int array[] = new int[10];    
            array[10] = 30/0;    
        } catch(ArithmeticException e) {  
            System.out.println(e.getMessage());  
        } catch(ArrayIndexOutOfBoundsException e) {  
            System.out.println(e.getMessage());  
        } catch(Exception e) {  
            System.out.println(e.getMessage());  
        }    
    }    
}  
-------------------------------Answer--------------------------------------------------------
  * In Java 7 it was made possible to catch multiple different exceptions in the same catch block. 
  * This is also known as multi catch.
  * The two exception class names in the first catch block are separated by the pipe character |. 
  * The pipe character between exception class names are how you declare multiple exceptions to be caught by 
the same catch clause.
------------------------------Program--------------------------------------------------------
*/


public class MultipleException {    
    public static void main(String[] args) {    
        try {    
            int array[] = new int[10];    
            array[10] = 10/0;    
        } catch(ArithmeticException | ArrayIndexOutOfBoundsException e) {  
            System.out.println(e.getMessage());  
        } catch(Exception e) { 
            System.out.println(e.getMessage());  
        }
    }    
}