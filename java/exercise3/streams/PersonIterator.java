/*
10.Iterate the roster list in Persons class and and print the person
without using forLoop/Stream 

-------------------------------------WBS-----------------------------------------

1.Requirement:
   - Iterate the roster list in Persons class and and print the person without using forLoop/Stream 
2.Entity:
  - PrintPersonIterator
3.Function declaration
  - public static void main(String[] args)

4.Jobs to be done:
   1.Invoke Person class createRoster method and store it in persons List.
   2.Print persons list using iterator method
      2.1)Check list contain next person using iterator method with hasNext method.
      2.2)Print Person class printPerson method using next method.

Psudeo Code:
''''''''''''
public class PersonIterator {
    public static void main(String[] args) {
        List<Person> persons = Person.createRoster();
        Iterator<Person> iterator = persons.iterator();
        while (iterator.hasNext()) {
            iterator.next().printPerson();
        }
    }
}
-------------------------------------Program-----------------------------------------
*/


import java.util.Iterator;
import java.util.List;

public class PersonIterator {

    public static void main(String[] args) {
        List<Person> persons = Person.createRoster();
        Iterator<Person> iterator = persons.iterator();
        // creating a object of iterator
        while (iterator.hasNext()) {
            iterator.next().printPerson();
            // next returns object of person
            //printPerson() prints the person name and age
        }
    }
}
