/*
1) Write a program to filter the Person, who are male and age greater than 21
-------------------------------WBS----------------------------------------

1.Requirement:
   - Write a program to filter the Person, who are male and age greater than 21
2.Entity:
   - FilterByGenderAndAge
3.Function declaration
   - public static void main(String[] args)
4.Jobs to be done:
    1.Invoke Person class createRoster method and store it in persons List.
    2.For each age person
          2.1)Check if Person's gender is MALE and age is greater than 21.
          2.2)Convert it as list using asList method.
    3.Print filteredPersons by String using toString method.        

Psudeo Code:
''''''''''''
public class FilteredPerson {
    public static void main(String[] args) {
    	 List<Person> persons = Person.createRoster();
	        List<Person> filteredPersons = persons.stream()
	        		                              .filter(person -> person.gender == Person.Sex.MALE )
	        		                              .filter(person -> person.getAge() > 21)
	        		                              .collect(Collectors.toList());
			System.out.println(filteredPersons.toString());
    }
---------------------------------Program--------------------------------------
*/

import java.util.List;
import java.util.stream.Collectors;

public class FilteredPerson {
    public static void main(String[] args) {
    	 List<Person> persons = Person.createRoster();
	        List<Person> filteredPersons = persons.stream()
	        		                              .filter(person -> person.gender == Person.Sex.MALE )
	        		                              .filter(person -> person.getAge() > 21)
	        		                              .collect(Collectors.toList());
			System.out.println(filteredPersons.toString());
    }
}
