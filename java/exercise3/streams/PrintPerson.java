/*
8. Print all the persons in the roster using java.util.Stream<T>#forEach 

-------------------------------------WBS---------------------------------------

1.Requirement:
	- Print all the persons in the roster using java.util.Stream<T>#forEach

2.Entity:
    - PrintPersonForEach

3.Function declaration
	- public static void main(String[] args)

4.Jobs to be done:
    1.Invoke Person class createRoster method and store it in personList List.
    2.For each personList print person name and age using Person class printPerson method

Psudeo Code:
''''''''''''
public class PrintPerson {
    public static void main(String[] args) { 
        List<Person> personList = Person.createRoster();
        personList.forEach(Person::printPerson);
    }
}
-------------------------------------Program---------------------------------------
*/

import java.util.List;

public class PrintPerson {

    public static void main(String[] args) {
    	// forEach accepts the printPerson method of Person class 
        //as Method reference as parameter 
        List<Person> personList = Person.createRoster();
        personList.forEach(Person::printPerson);
    }
}
