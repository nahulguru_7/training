/*
6. Write a program to find the average age of all the Person in the person List

-------------------------------------WBS-----------------------------------------

1.Requirement:
   - Write a program to find the average age of all the Person in the person List

2.Entity:
   - AverageAge

3.Function declaration
   - public static void main(String[] args)

4.Jobs to be done:
   1.Invoke Person class createRoster method and store it in persons List.
   2.For each age
            2.1)Get all person's Age using Person class getAge method.
            2.2)Averge all person's Age using average method.
            2.3)Get all person's Age in double using getAsDouble method.
   
Psudeo Code:
''''''''''''
public class AverageAgePerson {
    public static void main(String[] args) {
        List<Person> persons = Person.createRoster();
        double age = persons.stream()
        		               .mapToDouble(Person::getAge)
        		               .average() 
        		               .getAsDouble();
        System.out.println("Average age of all persons: " + age);
    }
}
-------------------------------------Program-----------------------------------------
*/

import java.util.List;

public class AverageAgePerson {

    public static void main(String[] args) {
    	//Stream method used to pipeline various methods
        //With the help of getAge gets age of all objects
        List<Person> persons = Person.createRoster();
        double age = persons.stream()
        		               .mapToDouble(Person::getAge)
        		               .average() //average() method calculates the average for sequential elements
        		               .getAsDouble(); //getAsDouble() method converts and return Optional double to double       
        System.out.println("Average age of all persons: " + age);
    }

}
