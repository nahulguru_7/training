/*
3. Write a program to filter the Person, who are male and
        - find the first person from the filtered persons
        - find the last person from the filtered persons
        - find random person from the filtered persons

-------------------------------------WBS-----------------------------------------

1.Requirement:

   - Write a program to filter the Person, who are male and
        - find the first person from the filtered persons
        - find the last person from the filtered persons
        - find random person from the filtered persons

2.Entity:
   - StreamFilterDemo

3.Function declaration
   - public static void main(String[] args)

4.Jobs to be done:
    1.Invoke Person class createRoster method and store it in rosterList List.
    2.Filter person's gender is Male and collect as list using asList method.
    3.Find the first person using get method.
    4.Find the last person subracting size of list by one using Person class printPerson method.
    5.Find random person get method with random nextInt using Person class printPerson method.
    
Psudeo Code:
''''''''''''
public class StreamFilterDemo {

    public static void main(String[] args) {
        List<Person> rosterList = Person.createRoster();
        List<Person> maleRosterList = rosterList.stream()
                                                .filter(aPerson -> aPerson.gender == Person.Sex.MALE)
                                                .collect(Collectors.toList());
        maleRosterList.get(0).printPerson();
        maleRosterList.get(maleRosterList.size() - 1).printPerson();
        Random random = new Random();
        maleRosterList.get(random.nextInt(maleRosterList.size())).printPerson();
    }
}
-------------------------------------Program-----------------------------------------

*/



import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class StreamFilterDemo {

    public static void main(String[] args) {
        List<Person> persons = Person.createRoster();
        //creating a roster  of persons

        List<Person> maleList = persons.stream()
                                                .filter(aPerson -> aPerson.gender == Person.Sex.MALE)
                                                .collect(Collectors.toList());
        // Stream API filter getting the persons who are male.

        maleList.get(0).printPerson();
        //getting the first person in the male list

        maleList.get(maleList.size() - 1).printPerson();
        //getting the last person in the male list

        Random random = new Random();
        maleList.get(random.nextInt(maleList.size())).printPerson();
        //using the object of Random class getting a random number given the range
        //as list size
        //the printPerson() method prints the person name along with his age.
    }

}
