/*
4. Consider a following code snippet:
    - List<Integer> randomNumbers = Array.asList({1, 6, 10, 25, 78})
    - Find the sum of all the numbers in the list using java.util.Stream API
    - Find the maximum of all the numbers in the list using java.util.Stream API
    - Find the minimum of all the numbers in the list using java.util.Stream API

-----------------------------------------------WBS---------------------------------------------

1.Requirement:
   - Consider a following code snippet:
    - List<Integer> randomNumbers = Array.asList({1, 6, 10, 25, 78})
    - Find the sum of all the numbers in the list using java.util.Stream API
    - Find the maximum of all the numbers in the list using java.util.Stream API
    - Find the minimum of all the numbers in the list using java.util.Stream API

2.Entity:
	- MaxMinSum

3.Function declaration
	- public static void main(String[] args)

4.Jobs to be done:
     1.Create a randomNumbers list and store it in Array elements as list using asList method.
     2.Find integer values using stream class mapToInt method ,intValue method and sum all 
integer using sum method and store it in sum integer.
     3.Find integer values using stream class mapToInt method ,getAsInt method and find the maximum
value in the list using max method and store it in maxList integer.
     4.Find integer values using stream class mapToInt method ,getAsInt method and find the minimum
value in the list using min method and store it in minList integer.
     5.Print the sum , minInList and maxInList.
     
Psudeo Code:
''''''''''''
public class MaxMinSum {

    public static void main(String[] args) {
        List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 25, 78);
        int sum = randomNumbers.stream().mapToInt(Integer::intValue).sum();
        int maxInList = randomNumbers.stream()
                .mapToInt(Integer::intValue).max().getAsInt();
        int minInList = randomNumbers.stream()
                .mapToInt(Integer::intValue).min().getAsInt();
        System.out.println("Sum: " + sum);
        System.out.println("Minimum in list: " + minInList);
        System.out.println("Maximum in list: " + maxInList);
    }
}
-----------------------------------------------Program---------------------------------------------*/


import java.util.Arrays;
import java.util.List;

public class MaxMinSum {

    public static void main(String[] args) {
        List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 25, 78);
        // list of numbers

        int sum = randomNumbers.stream().mapToInt(Integer::intValue).sum();
        /* stream method used to get the desired result with the pipelined methods
         * mapToInt method helps to convert into integer values
         * sum method sums the integer values and return int value.
         */

        int maxInList = randomNumbers.stream()
                .mapToInt(Integer::intValue).max().getAsInt();
        // max method returns maximum element from stream
        // getAsInt method used to change the type to int from optionalInt

        int minInList = randomNumbers.stream()
                .mapToInt(Integer::intValue).min().getAsInt();
        // max method returns maximum element from stream
        // getAsInt method used to change the type to int from optionalInt

        System.out.println("Sum: " + sum);
        System.out.println("Minimum in list: " + minInList);
        System.out.println("Maximum in list: " + maxInList);
    }

}
