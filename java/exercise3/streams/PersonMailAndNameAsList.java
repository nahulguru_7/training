/*

5. Write a program to collect the minimal person with name and email address
from the Person class using java.util.Stream<T> API as List

-----------------------------------WBS------------------------------------------

1.Requirement:
    - Write a program to collect the minimal person with name and email address
from the Person class using java.util.Stream<T> API as List

2.Entity:
    - PersonMailAndNameAsList

3.Function Declaration
    - public static void main(String[] args)

4.Jobs to be done:
     1.Invoke the createRoster method using the Person class and store it in roster list.
     2.Get the person's email using Person class getEmailAddress and collect as list using asList method 
store it in emailRoster list.
     3.Check if the get email is present in roster list.
              3.1)Print the person's name and email.
     
     
Pseudo Code:
''''''''''''
public class PersonMailAndNameAsList {
    public static void main(String[] args) {
        List<Person> roster = person.createRoster();
        List<String> emailList = roster.stream().map(Person::getEmailAddress)
        .collect(Collectors.toList());
        if (roster.email == emailList.email) {
            roster.name
        }
    }
}
*/

import java.util.List;
import java.util.stream.Collectors;

public class PersonMailAndNameAsList {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        // creating a list and storing the returned list

        List<String> emailRoster = roster.stream()
            .map(Person::getEmailAddress)
            .collect(Collectors.toList());
        // created a list of emails from objects of person class using pipelined

        for (int i = 0; i < emailRoster.size(); i++) {
            for (int j = 0; j < roster.size(); j++) {
                if (roster.get(j).emailAddress == emailRoster.get(i)) {
                    System.out.println(
                        "Person name: " +
                        roster.get(j).name + ", mail Id: " +
                        emailRoster.get(i)
                    );
                }
            }
        }
        // using email to match the email in roster list to
        // get the name of the person.
    }
}
