/*
 ----------------------------------------Word Breakdown Structure(WBS)--------------------------------------------------------
1.Requirements
   - Convert the following anonymous class into lambda expression.
2.Entities
   - EvenOrOdd
   - CheckNumber (Interface)
3.Function Declaration
   - int numbers(int number1,int number2);
   - public static void main(String[] args)
4.Jobs to be done
   1.Declare the single method with integer variable value parameters
   2.Create interface object and assign with passing int value.
        2.1)Declare interfce object and initailise class object reference display method
        2.2)Using interface object reference and invoke interface printString method

Psudeo Code:
''''''''''''
interface Printer {
    public void printString(String string);
}

class Reference {
    public void display(String string){
        System.out.println(string);
    }
    public static void main(String[] args ) {
    	Reference object = new Reference();
        Printer printer = object::display;
        printer.printString("Instance Method Reference");
    }
}

-----------------------------------------------Program---------------------------------------------------*/

interface Printer {
    public void printString(String string);
}

class Reference {
    
    public void display(String string){
        System.out.println(string);
    }
    public static void main(String[] args ) {
        
        //Creating an object for class InstanceReference
    	Reference object = new Reference();
        
                        //ObjectOfClass::instanceMethod
        Printer printer = object::display;
        
        //Invoking interface method
        printer.printString("Instance Method Reference");
    }
}