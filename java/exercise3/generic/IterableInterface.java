/*Generics-Implementing Iterable interface:
1. Write a program to print employees name list by implementing iterable interface.

--------------------------------WBS---------------------------------
 1.Requirements:
  		To write a program to print employees name list by implementing iterable interface.
 2.Entity:
  	  - MyIterable
  	  - IterableInterface
  
 3.Function Declaration:
  	  - public MyIterable(T[] t),
  	  -	public Iterator<T> iterator().
  
 4.Jobs To Be Done:
  		1.Create a method MyIterable of generic type  and assign already defined String values from the main class.
  		3.Declare the String values in the name variable.
  		4.Creatie the object myList for the variables to be iterated.
  		5.After assign the variables in the list by iterator implement, the final result is printed.

Psudeo Code:
''''''''''''
class MyIterable<T> implements Iterable<T> {
	private List<T> list;
	public MyIterable(T[] t) {
		list = Arrays.asList(t);
	}
	public Iterator<T> iterator() {
		return list.iterator();
	}
}
public class IterableInterface {
	public static void main(String[] args) {
		String[] apps = {"Varun", "Barath", "Prabha", "Dharun", "Kala"};
		MyIterable<String> app = new MyIterable<>(apps);
		for (String string : app) {
			System.out.println(string);
		}
	}
}

----------------------------Program----------------------------------
 */

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

class MyIterable<T> implements Iterable<T> {
	private List<T> list;
	public MyIterable(T[] t) {
		list = Arrays.asList(t);
	}
	public Iterator<T> iterator() {
		return list.iterator();
	}
}
public class IterableInterface {
	public static void main(String[] args) {
		String[] apps = {"Varun", "Barath", "Prabha", "Dharun", "Kala"};
		MyIterable<String> app = new MyIterable<>(apps);
		for (String string : app) {
			System.out.println(string);
		}
	}
}