/*
Generics-Class literals:
2. Write a program to demonstrate generics - for loop, for list, set and map.

-------------------------------------------WBS----------------------------------------------------

1.Requirement:
    Java program to demonstrate adding elements, displaying, removing, and iterating in hash set.

2.Entity:
   - HashSetDemo

3.Function Declaration
   - public static void main(String[] args)

4.Jobs to be done:
   1.Create a Set and add Set elements uadd method and print the names set.
   2.Remove Specified element in set using remove() method.
   3.Display set elements using for each.


Psudeo Code:
'''''''''''
public class HashSetDemo {
    public static void main(String[] args) {
        Set<String> names = new HashSet<>();
        ADD 5 ELEMENTS
        System.out.println(names);
        System.out.println("Item removed: " + names.remove("Prakash"));
        for (String name : names) {
            System.out.println(name);
        }
    }
}
------------------------------------Program Code-----------------------------------------------
*/
import java.util.HashSet;
import java.util.Set;

public class HashSetDemo {

    public static void main(String[] args) {

        Set<String> names = new HashSet<>();
        // creating a set
        names.add("Santheesh");
        names.add("Vasanth");
        names.add("Senthil");
        names.add("Prakash");
        names.add("Harish");
        // adding elements to set

        System.out.println(names);
        // displaying the set

        System.out.println("Item removed: " + names.remove("Prakash"));
        // removing an element

        for (String name : names) {
            System.out.println(name);
        }
        // iterating elements in set

    }

}
