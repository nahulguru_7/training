/*3. Write a generic method to exchange the positions of two different elements in an array.
 -----------------------------WBS----------------------------------------
1.Requirement:
    - Write a generic Method to swap the elements.

2.Entity:
    - public class SwapDemo

3.Function Declaration:
    - public static <T> void swap(T[] list, int firstNumber, int lastNumber)
    - public static <T> void printSwap(List<T> list1, int firstNumber, int secondNumber)
    - public static void main(String[] args)
4.Jobs to be done:
    1.Create a swap method of type T and swap the elements using temperary variables.
    2.Create another printSwap method and swap by using swap method.
    3.Create an array and invoke the swap method and print the result.
    4.Create an object for List of type Integer.
    5.Invoke ExchangePosition class printSwap method ad print the result.
    
Psudeo Code:
''''''''''''
public class ExchangePosition {
    public static <T> void swap(T[] list, int firstNumber, int lastNumber) {
        T temperaryVariable = list[firstNumber];
        list[firstNumber] = list[lastNumber];
        list[lastNumber] = temperaryVariable;
        System.out.println("The swapped element is " + Arrays.toString(list) );
    }
	public static <T> void printSwap(List<T> list1, int firstNumber, int secondNumber) {
        Collections.<T> swap(list1, firstNumber, secondNumber);
    }
    public static void main(String[] args) {
        Integer[] list = {110, 250, 300, 234, 540, 600};
        swap(list, 2, 4);
        List<Integer> list1 = new ArrayList<>(Arrays.asList(list));
        printSwap(list1, 2, 4);
        System.out.println(list1);
    }
}
-----------------------------Program----------------------------------*/

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ExchangePosition {
    
    public static <T> void swap(T[] list, int firstNumber, int lastNumber) {
        T temperaryVariable = list[firstNumber];
        list[firstNumber] = list[lastNumber];
        list[lastNumber] = temperaryVariable;
        System.out.println("The swapped element is " + Arrays.toString(list) );
    }
    @SuppressWarnings("unused")
	public static <T> void printSwap(List<T> list1, int firstNumber, int secondNumber) {
        Collections.<T> swap(list1, firstNumber, secondNumber);
    }
    
    public static void main(String[] args) {
    	//Declare and adding elements in lists
        Integer[] list = {110, 250, 300, 234, 540, 600};
        swap(list, 2, 4);
        
        List<Integer> list1 = new ArrayList<>(Arrays.asList(list));
        //Swapping particular selected elements
        printSwap(list1, 2, 4);
        System.out.println(list1);
        
    }
}

