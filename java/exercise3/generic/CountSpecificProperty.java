/*1. Write a generic method to count the number of elements in a collection that have a specific
   property (for example, odd integers, prime numbers, palindromes).

 ------------------------------WBS------------------------------
 1.Requirements : 
 		Write a generic method to count the number of elements in a collection that have a specific
 property (for example, odd integers, prime numbers, palindromes).
 
 2.Entities :
 	- public class CountSpecificProperty.
 3.Function Declaration :
 	- public static void main(String[] args)
 4.Jobs To Be Done:
 		1.Create the count method which returns the count of odd numbers present in a list.
 		2.Create the main method and create a list reference.
 		3.Add elements inside a list.	
 		4.Invoke a count method and print the number of odd numbers.
 		
Psudeo Code:
''''''''''''

public class CountSpecificProperty {
	public static int count(ArrayList<Integer> list) {
		int c = 0;
		for(int elements : list) {
			if(elements % 2 != 0) {
				c++;
			}
		}
		return c;
	}
	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		ADD 5 ELEMENTS 
		System.out.println("Number of odd integers : " + count(list));

	}

}

-----------------------------Program------------------------------
 */

import java.util.ArrayList;

public class CountSpecificProperty {
	
	public static int count(ArrayList<Integer> list) {
		int c = 0;
		for(int elements : list) {
			if(elements % 2 != 0) {
				c++;
			}
		}
		return c;
	}

	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		//Adding elements using add() method
		list.add(20);
		list.add(41);
		list.add(13);
		list.add(12);
		list.add(91);
		//Printing the count of list using count() method
		System.out.println("Number of odd integers : " + count(list));

	}

}
