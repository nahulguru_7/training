/*
 5.Demonstrate java program to working of map interface using( put(), remove(), booleanContainsValue(), replace() ) .

----------------------------------------------WBS--------------------------------------------------
1.Requirements:
   - Java program to working of map interface using( put(), remove(), booleanContainsValue(), replace()).
2.Entities
   - MapInterface
3.Function Declaration
   - public static void main(String[] args)
4.Jobs to be done
   1.Create a Class name TreeSetDemo and declare main method.
   2.Create a TreeMap as car.
   3.Use put() to add elements, remove() to remove element, booleanContainsValue() to check element, replace() methods to replace element 
   4.Print the cars. 
 
----------------------------------------------Program--------------------------------------------------*/
import java.util.TreeMap;
public class MapInterface {
	public static void main(String[] args) {				
	      TreeMap<Integer, String> cars = new TreeMap<Integer, String>();  //Creating TreeMap and SortedMap
	      cars.put(2, "Tesla");   //Using put() method
	      cars.put(3, "BMW");
	      cars.put(5, "Honda ");
	      cars.put(1,  "Lambogini");
		  System.out.println(cars);
	      cars.remove(3, "BMW");  //Using remove() method
	      cars.replace(1,"Lambogini");  //Using replace() method
	      System.out.println(cars.containsValue("Lexus")); //Using containsValue() method
		  System.out.println(cars);
	}
}
