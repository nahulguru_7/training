/*
5. Write some String content using Writer

---------------------------WBS-------------------------------------

1.Requirement:
   - Write some String content using Writer

2.Entity:
   - WriteStringUsingWriter

3.Method Signature:
   - public static void main(String[] args)

4.Jobs to be done:

   1.Get the file using file path and store file in source String.
      1.1)Check file exist using Writer class.
   2.Store line in text String  
   2.Write the text String in file using FileWriter 
   3.Invoke the writer method to write content to file and close the writer.

Pseudo Code:
''''''''''''
public class WriteStringUsingWriter {

    public static void main(String[] args) {
        Writer writer = new FileWriter(source);
        String text = "This Content writtern by using Writer";
        writer.write(text);
        writer.close();
        System.out.println("File writed successfully");
    }

}
---------------------------Program Code-------------------------------------
*/

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class UseWriter {

    public static void main(String[] args) {

        // source as file path
        String source = "C:/Users/santh/eclipse-workspace/JavaEE-Demo/io.stream/Content";
        try {
            // FileWriter for writing data to file
            Writer writer = new FileWriter(source);
            String text = "This Content writtern by using Writer";
            writer.write(text);
            writer.close();
            System.out.println("File writed successfully");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}