/*
1. Reading a file using InputStream

---------------------------WBS-------------------------------------

1.Requirement:
   - Read a file using InputStream

2.Entity:
  - InputStreamDemo

3.Method Signature:
  - public static void main(String[] args)

4.Jobs to be done:
     1.Get the file in InputStream using file path to read data byte by byte. 
      1.1)Store it in source String.
      1.2)Check file exist using FileInputStream class.
     2.Read method to read data byte by byte.
         2.1) Check the file data less than zero, It assumes there is no more data in that file.
         2.2)Data present convert the integer to character.

Pseudo Code:
''''''''''''
public class InputStreamDemo {

    public static void main(String[] args) throws IOException {
        InputStream input = new FileInputStream(source); 
        int character;
        while ((character = input.read()) != -1) {
            System.out.println((char) data);
        }
    }
}
---------------------------Program Code-------------------------------------
*/

import java.io.FileInputStream;
import java.io.IOException;

public class ReadInputStream {

    public static void main(String[] args) throws IOException {
        String source = "C:/Users/santh/eclipse-workspace/JavaEE-Demo/io.stream/Content";
        try {
            // get the file through input stream
            // input stream get data byte by byte
            @SuppressWarnings("resource")
			FileInputStream file = new FileInputStream(source);
            int character;

            // read to get a single byte
            while ((character = file.read()) != -1) {
                System.out.println((char) character);
            }
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }

}