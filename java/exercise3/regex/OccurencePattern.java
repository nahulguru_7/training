/*
Problem Statement:
    Find the no of occurrence of a pattern in the text and also fetch the start and 
end index of the the occurrence.
	  
------------------------------------------WBS---------------------------------

1.Requirement :
	- Find the no of occurrence of a pattern in the text and also fetch the start and 
end index of the the occurrence.

2.Entities :
	- OccurencePattern

3.Function declaration:
    - public static void main(String[] args) 
	
3.Job to be Done:
	1.Create string sentence and store string pattern.
	2.Set pattern compile method and store it in pattern.
	3.Match the sentence and pattern using matcher method store it in matcher.
	4.Check matcher using find method and print group using group method start using start method.

Psudeo Code:
''''''''''''
public class OccurencePattern {
	public static void main(String[] args)  { 
	        String sentence = "abaabbaaabbbaaaabab";
	        Pattern pattern = Pattern.compile("a{2,5}");
	        Matcher matcher = pattern.matcher(sentence);   
	        while(matcher.find()){
	            System.out.println(matcher.group()+" \tstarts at "+matcher.start());
	        }    
	}
}
	
--------------------------------Program---------------------------------------------
*/

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OccurencePattern {
	
	public static void main(String[] args)  { 
	        String sentence = "abaabbaaabbbaaaabab";
	        Pattern pattern = Pattern.compile("a{2,5}");
	        Matcher matcher = pattern.matcher(sentence);   
	        while(matcher.find()){
	            System.out.println(matcher.group()+" \tstarts at "+matcher.start());
	        }    
	}
}
