/* Regex
   *-*-*
Problem Statement:
''''''''''''''''''

7.Split any random text using any pattern you desire
--------------------------------------WBS---------------------------------------------
1.Requirements:
   - Split any random text using any pattern you desire
2.Entities
   - SplitText
3.Function Declaration
   - public static void main (String [] args)
4.Jobs to be done
  1.Create a text type String and a string in it.
  2.Create a splitString type String list and store split text with regex condition.
  3.Print the splited array splitString by for loop.
  
Psudeo Code:
''''''''''''
public class SplitText{  
	public static void main(String[] args){  
		String text ="split any random text using any pattern you desire"; 
		String[] splitString = text.split("\\s", 5);
		for (int i=0; i < splitString.length; i++){
			System.out.println(splitString[i]);
    }
  }
}
--------------------------------Program------------------------------------------------*/


public class SplitText{  
	public static void main(String[] args){  
		String text ="split any random text using any pattern you desire"; 
		String[] splitString = text.split("\\s", 5);
		for (int i=0; i < splitString.length; i++){
			System.out.println(splitString[i]);
    }
  }
}