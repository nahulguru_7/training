/*
 Regex Methods:
 =============

Problem Statement:

2. For the following code use the split method() and print in sentance
    String website = "https-www-google-com";
    
-------------------------WBS-----------------------------

1.Requirement :
	- Program for Java String Regex Methods.

2.Entities :
	- SplitMethod

3.Method Signature:
    - public static void main(String[] args)
    
4.Job to be Done :
	1.Create website type String and store a website string with "-".
	2.Split the string "-" using split method
	      2.1)Print each string using for each.
	      
Psudeo Code:
''''''''''''

public class SplitMethod {
	
	public static void main(String[] args) {
		String website = "https-www-kpriet-com";
	    for (String display: website.split("-")) {
	         System.out.print(display+" ");
	    }  
	}
}
-------------------------Program Code-------------------------------
*/


public class SplitMethod {
	
	public static void main(String[] args) {
		String website = "https-www-kpriet-com";
	    for (String display: website.split("-")) {
	         System.out.print(display+" ");
	    }  
	}
}
