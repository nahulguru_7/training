/*
Quantifiers
===========

Problem Statement:

3.)Create a username for login website which contains
   8-12 characters in length
   Must have at least one uppercase letter
   Must have at least one lower case letter
   Must have at least one digit

   
----------------------------------------WBS----------------------------------------

1.Requirement :
	 Create a username for login website which contains
	   8-12 characters in length
	   Must have at least one uppercase letter
	   Must have at least one lower case letter
	   Must have at least one digit

2.Entities :
	UserNameValidation
	
3.Job to be Done :
	1.Create userName type String and store it in an userName.
	2.Using pattern class and check the userName condition in compile method.
	3.Using matcher class check pattern class matches the userName.
	4.Check matcher using find method and print userName is valid or not.

Psudeo Code:
''''''''''''
public class UserNameValidation {
	 
    public static void main(String[] args) {
        
        String userName = "Santhees62";
        Pattern pattern = Pattern.compile("(?=.*[0-9])(?=.*[a-z])(?=:*[A-Z]).{8,12}");
        Matcher matcher = pattern.matcher(userName);   
        if(matcher.find()){
            System.out.println(userName + " is valid UserName");
        }
        else {
            System.out.println(userName + " is not valid UserName");
        }
    }
}

----------------------------------------Program Code----------------------------------------
*/




import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserNameValidation {
	 
    public static void main(String[] args) {
        
        String userName = "Santhees62";
        Pattern pattern = Pattern.compile("(?=.*[0-9])(?=.*[a-z])(?=:*[A-Z]).{8,12}");
        Matcher matcher = pattern.matcher(userName);   
        if(matcher.find()){
            System.out.println(userName + " is valid UserName");
        }
        else {
            System.out.println(userName + " is not valid UserName");
        }
    }
}
