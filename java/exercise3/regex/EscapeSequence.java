/*
Problem Statement:
2.)Write a program to display the following using escape sequence in java
    A.My favorite book is "Twilight" by Stephanie Meyer
    B.She walks in beauty, like the night, 
	  Of cloudless climes and starry skies 
	  And all that's best of dark and bright 
	  Meet in her aspect and her eyes�
    C."Escaping characters", � 2019 Java
	  
----------------------------------WBS--------------------------------------------
1.Requirement :
	- Program to display the following using escape sequence in java
	    A.My favorite book is "Twilight" by Stephanie Meyer
	    B.She walks in beauty, like the night, 
		  Of cloudless climes and starry skies 
		  And all that's best of dark and bright 
		  Meet in her aspect and her eyes�
	    C."Escaping characters", � 2019 Java
	  
2.Entities :
	- EscapeSequence
	
3.Method Signature:

	
	
4.Job to be Done :
	1.Create myFavoriteBook string and store A part in it using Escape Sequence.
	    1.1)Print it using myFavoriteBook.
    2.Create sentence string and store B part in it using Escape Sequence.
	    2.1)Print it using sentence.
	3.Print C part using Escape Sequence.

Psudeo Code:
'''''''''''
        String myFavoriteBook = new String (A);
	    System.out.println("A: "+myFavoriteBook);
	    
	    String sentence = new String (B);    
	    System.out.println("\nB: "+ sentence);
	    
	    System.out.println(C); 

----------------------------------Prgram Code--------------------------------------------*/


public class EscapeSequence {
	
	public static void main(String[] args) {
		
		String myFavoriteBook = new String ("My favorite book is \"Twilight\" by Stephanie Meyer");
	    System.out.println("A: "+myFavoriteBook);
	    
	    String sentence = new String ("She walks in beauty, like the night, \nOf cloudless climes and starry skies\nAnd all that's best of dark and bright\nMeet in her aspect and her eyes...");    
	    System.out.println("\nB: "+ sentence);
	    
	    System.out.println("\nc: \"Escaping characters\", \u00A9 2019 CodeGym");
	    
	}
}
