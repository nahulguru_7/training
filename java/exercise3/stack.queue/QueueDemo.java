/* Create a queue using generic type and in both implementation Priority Queue,Linked list and complete following  
  -> add atleast 5 elements
  -> remove the front element
  -> search a element in stack using contains key word and print boolean value value
  -> print the size of queue
  -> print the elements using Stream 
---------------------------------------------------------WBS---------------------------------------------------
1.Requirements :
  - Create a stack using generic type and implement
  -> Push atleast 5 elements
  -> Pop the peek element
  -> search a element in stack and print index value
  -> print the size of stack
  -> print the elements using Stream
  Reverse List Using Stack with minimum 7 elements in list.

2.Entities:
  - QueueDemo 
3.Function Declaration:
  - linkedList()
  - priority
  - public static void main(String [] args)
4.Job to be done :  
   1.Invoke Queue class linkedList method and priority method.
   2.Create Linkedlist in linkedList method.
       2.1)Add 5 elements using add method
       2.2)Check the linkedlist containing a particular String or not using conatins method.
       2.3)Print the size of linkedlist usng size method.
       2.4)Print all linkedlist element using stream and for each.
   3.Create PriorityQueue in priorityQueue method.
       3.1)Add 5 elements in PriorityQueue.
       3.2)Check the priorityqueue containing a particular String or not using conatins method.
       3.3)Print the size of priorityqueue usng size method.
       3.4)Print all priorityqueue element using stream and for each.
 
Psudeo Code:
''''''''''''
public class QueueDemo{
	public void linkedList() {
		 Queue<String> cars = new LinkedList<String>();
	        Stream<String> stream = cars.stream();
	        ADD 5 ELEMENTS
	        System.out.println(cars.contains("Tesla"));
	        System.out.println(cars.size());       
	        stream.forEach((element) -> {
	           System.out.println(element);  
	        }); 
	}
	public void priority() {
		 Queue<String> cars = new PriorityQueue<String>();
	        Stream<String> stream = cars.stream();
	        ADD 5 ELEMENTS
	        System.out.println(cars.contains("Honda"));
	        System.out.println(cars.size());   
	        stream.forEach((element) -> {
	           System.out.println(element); 
	        });   
	    }
    public static void main(String [] args) {
    	QueueDemo queue = new QueueDemo();
    	queue.linkedList();
    	queue.priority();
    }
}
 
 ----------------------------------------------------Program Code---------------------------------------------------
*/
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Stream;
public class QueueDemo{
	public void linkedList() {
		 Queue<String> cars = new LinkedList<String>();
	        Stream<String> stream = cars.stream();
	        cars.add("BMW");
	        cars.add("Mazda");
	        cars.add("Tesla");
	        cars.add("Lexus");
	        cars.add("Meclaren");
	        cars.remove();
	        
	        System.out.println(cars.contains("Tesla"));
	        System.out.println(cars.size());       
	        stream.forEach((element) -> {
	           System.out.println(element);  
	        }); 
	}
	public void priority() {
		 Queue<String> cars = new PriorityQueue<String>();
	        Stream<String> stream = cars.stream();
	        cars.add("BMW");        
	        cars.add("Mazda");
	        cars.add("Tesla");
	        cars.add("Lexus");
	        cars.add("Meclaren");
	        cars.remove();
	        System.out.println(cars.contains("Honda"));
	        System.out.println(cars.size());   
	        stream.forEach((element) -> {
	           System.out.println(element); 
	        });    //Process Stack Using Stream
	    }
    public static void main(String [] args) {
    	QueueDemo queue = new QueueDemo();
    	System.out.println("-----------------Linked List----------------------------");
    	queue.linkedList();
    	System.out.println("-----------------Priority----------------------------");
    	queue.priority();
    }
}