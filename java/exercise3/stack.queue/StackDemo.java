/*  Create a stack using generic type and implement
  -> Push atleast 5 elements
  -> Pop the peek element
  -> search a element in stack and print index value
  -> print the size of stack
  -> print the elements using Stream
  Reverse List Using Stack with minimum 7 elements in list.

---------------------------------------------------------WBS---------------------------------------------------
1.Requirements :
  - Create a stack using generic type and implement
  -> Push atleast 5 elements
  -> Pop the peek element
  -> search a element in stack and print index value
  -> print the size of stack
  -> print the elements using Stream
  Reverse List Using Stack with minimum 7 elements in list.
2.Entities:
  - StackDemo 
3.Function Declaration:
  - public static void main(String [] args)
4.Job to be done :  
   1.Create the Stack with generic String type and create stream for print the Stack elements.
   2.Push 5 values to stack using push method and using pop method print top value in stack.
   3.Search the specified value using search method and find size of stack using size method.
   4.Print the index value in stack using specifying index.
   5.Print the stack values using stream and for each.
   6.Reverse a stack create another stack remove the values using remove method and push to new create
stack using push method 
   7.Remove elements from stack add to list using add method.

Psudeo Code:
''''''''''''
public class StackDemo {
    public static void main(String [] args) {
        Stack<String> softwares = new Stack<String>(); 
        Stream<String> stream = softwares.stream();
        ADD 5 ELEMENTS     
        softwares.pop();   
        int index = softwares.search("Mongodb");  
        int size = softwares.size();      
        System.out.println(size);        
        System.out.println(index);      
        stream.forEach((element) -> {
           System.out.println(element); 
        }); 
        Stack<String> stack = new Stack<String>();
        while(softwares.size() > 0) {
                  stack.push(softwares.remove(0)); 
        }
        while(stack.size() > 0){
                softwares.add(stack.pop()); 
        } 
        System.out.println(softwares);
    }
}
  
  ----------------------------------------------------Program Code---------------------------------------------------
*/
import java.util.Stack;
import java.util.stream.Stream;
public class StackDemo {
    public static void main(String [] args) {
        Stack<String> softwares = new Stack<String>();  //Cretaing Stack using generic type
        Stream<String> stream = softwares.stream();
        softwares.push("Chrome");
        softwares.push("Notepad++");
        softwares.push("Eclipse");
        softwares.push("WhatsApp"); 
        softwares.add("Telegram");        
        softwares.pop();   //pop top element in stack
        System.out.println("--------------------Create a stack using generic type and implement-----------------");
        int index = softwares.search("Mongodb");  //Initialise search variable
        int size = softwares.size();        //Initialise size variable
        System.out.println(size);       //Printing the stack size 
        System.out.println(index);      //Printing the index of the element
        stream.forEach((element) -> {
           System.out.println(element);  // print element
        });    //Process Stack Using Stream
        
        System.out.println("--------------------Reverse List Using Stack with minimum 7 elements in list-----------------");
        Stack<String> stack = new Stack<String>();
        while(softwares.size() > 0) {
                  stack.push(softwares.remove(0)); //Element is removed from list and push to stack
        }

        while(stack.size() > 0){
                softwares.add(stack.pop()); //Element is pop from stack and add to list
        } 

        System.out.println(softwares);
    }
}
/*
-------------------------------------------------------------Output---------------------------------------------------------------
--------------------Create a stack using generic type and implement-----------------
4
-1
Chrome
Notepad++
Eclipse
WhatsApp
--------------------Reverse List Using Stack with minimum 7 elements in list-----------------
[WhatsApp, Eclipse, Notepad++, Chrome]

*/