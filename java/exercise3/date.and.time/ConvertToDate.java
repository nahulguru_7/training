import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ConvertToDate {
	public static void main(String[] args) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 1);
		java.util.Date date = cal.getTime();             
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		String date1 = format1.format(date); 
		System.out.println(date1);
		@SuppressWarnings("unused")
		java.util.Date inActiveDate = null;
		try {
		    inActiveDate = format1.parse(date1);
		} catch (ParseException e1) {
		    // TODO Auto-generated catch block
		    e1.printStackTrace();
		}
	}
}
