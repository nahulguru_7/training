/*
10.Write a Java program to get a date before and after 1 year compares to the current date. 

----------------------------------------WBS---------------------------------------
1.Requirement :
    - Program to get a date before and after 1 year compares to the current date.

2.Entity:
    - BeforeAfterOneYear

3.Method Declaration:
    - public static void main(String[] args)

4.Jobs to be done :

Pseudo Code:
''''''''''''
public class BeforeAfterOneYear {
   public static void main(String[] args)
    {
      Calendar cal = Calendar.getInstance();
      Date cdate = cal.getTime();
      // get next year
      cal.add(Calendar.YEAR, 1); 
      Date nyear = cal.getTime();
      //get previous year
      cal.add(Calendar.YEAR, -2); 
      Date pyear = cal.getTime();
      System.out.println("\nCurrent Date : " + cdate);
      System.out.println("\nDate before 1 year : " + pyear);
      System.out.println("\nDate after 1 year  : " + nyear+"\n");  	
    }
}
---------------------------------Program Code--------------------------------*/
import java.util.Calendar;
import java.util.Date;
public class BeforeAfterOneYear {
   public static void main(String[] args)
    {
      Calendar cal = Calendar.getInstance();
      Date cdate = cal.getTime();
      // get next year
      cal.add(Calendar.YEAR, 1); 
      Date nyear = cal.getTime();
      //get previous year
      cal.add(Calendar.YEAR, -2); 
      Date pyear = cal.getTime();
      System.out.println("\nCurrent Date : " + cdate);
      System.out.println("\nDate before 1 year : " + pyear);
      System.out.println("\nDate after 1 year  : " + nyear+"\n");  	
    }
}
