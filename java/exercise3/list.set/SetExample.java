/*Create a list
    => Add 10 values in the list
    => Create another list and perform addAll() method with it
    => Find the index of some value with indexOf() and lastIndexOf()
    => Print the values in the list using 
        - For loop
        - For Each
        - Iterator
        - Stream API
    => Convert the list to a set
    => Convert the list to a array
+ Explain about contains(), subList(), retainAll() and give example

----------------------------------------Word Breakdown Structure(WBS)--------------------------------------------------------

1.Requirements
   - Program to print difference of two numbers using lambda expression and the single method interface
2.Entities
   - SetDemo
3.Function Declaration

4.Jobs to be done
   1.Create set as cars and add 10 cars using add() method and printing the added set.
   2.Create another set of newCars and adding 4 newCars using addAll() method adding cars and newCars sets and after using 
removeAll() method removing added newCars from cars set.
   3.Display the set using while loop with hasNext() and forEach.
   4.Check the set with contain() method to set containing or not and to check set is empty or not using isEmpty() method.

Psudeo Code:
''''''''''''
public class SetDemo {
	public static void main(String[] args) {
	    Set<String> cars = new HashSet<>();
	    ADD 10 ELEMETS
	    Set<String> newCars = new HashSet<>();
	    ADD ELEMENTS TO ANOTHER SET FROM SET
	    Iterator<String> car = cars.iterator();
        while (car.hasNext()) {
            System.out.println(car.next());
        }
        cars.forEach(System.out::println)
        System.out.println("Checks car set contains Tata Motors " + cars.contains("Tata Motors"));
        System.out.println("Checks newCars is empty or not? " + newCars.isEmpty());
	}
}
-----------------------------------------------Program--------------------------------------------------------------------------------*/


import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetExample {

	public static void main(String[] args) {
		
		//Creating a set and adding 10 elements to it.
		Set<String> cars = new HashSet<>();
        cars.add("BMW");
        cars.add("Audi");
        cars.add("Ferrari");
        cars.add("Honda");
        cars.add("Infiniti");
        cars.add("Jaguar");
        cars.add("Maserati");
        cars.add("Citroen");
        cars.add("Chevrolet");
        cars.add("Ford");
        System.out.println("Creating a set and adding 10 elements to it:\n" + cars );
        
        //Creating another set with some elements.
        Set<String> newCars = new HashSet<>();
        newCars.add("Tesla");
        newCars.add("Toyota");
        newCars.add("Volvo");
        newCars.add("Volkswagen");
        
        //Perform addAll() method and removeAll() to the set.
        cars.addAll(newCars);
        System.out.println("\t-------------addAll() method-------------------- ");
        System.out.println(cars);
        System.out.println("\t-------------removeAll() method-------------------- ");
        cars.removeAll(newCars);
        System.out.println(cars);
        newCars.clear();
        
        
      //Displaying all the set elements using Iterator interface
        Iterator<String> car = cars.iterator();
        System.out.println("\t------------------Using While loop---------------------");
        while (car.hasNext()) {
            System.out.println(car.next());
        }

        //Displaying all the set elements using ForEach
        System.out.println("\t-------------Using ForEach---------------");
        cars.forEach(System.out::println);

        //contains()
        System.out.println("\t------------------contains() method---------------------");
        System.out.println("Checks car set contains Tata Motors " + cars.contains("Tata Motors"));

        //isEmpty()
        System.out.println("\t------------------isEmpty() method---------------------");
        System.out.println("Checks newCars is empty or not? " + newCars.isEmpty());
	}

}
