/*
Problem Statement:
================= 
 Write a program of performing two tasks by two threads that implements Runnable interface.
 
------------------------------------------WBS-----------------------------------------
1.Requirements:
   - Program of performing two tasks by two threads that implements Runnable interface.
2.Entities
   - ThreadRun1
   - ThreadRun2
   - TwoThreadsRunnable
3.Function Declaration
   - public void run() 
   - public static void main (String [] args)
4.Jobs to be done:
  1.Create an object ThreadRun1 and ThreadRun2 class. 
  2.Pass argument of object in Thread constructor.
  3.Starts both threads and runs threads using run method.
  4.Both run method contains for loop with limit to print with sleep time.   
 
Psudeo Code:
''''''''''''
class ThreadRun1 implements Runnable {
	public void run() {
		for (int i = 0;i <= 5; i++) {
			try {
				System.out.println("Thread-1");
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

class ThreadRun2 implements Runnable {	
	public void run() {
		for (int i = 0;i <= 5; i++) {
		try {
			System.out.println("Thread-2");
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
  }
}

public class TwoThreadsRunnable {
	public static void main(String[] args) {	
		ThreadRun1 tOne = new ThreadRun1();
		ThreadRun2 tTwo = new ThreadRun2();
		Thread t1 = new Thread(tOne);
		Thread t2 = new Thread(tTwo);
		t1.start();
		t2.start();
	}
}  
------------------------------------------Program-----------------------------------------  */
class ThreadRun1 implements Runnable {
	
	public void run() {
		for (int i = 0;i <= 5; i++) {
			
			try {
				System.out.println("Thread-1");
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

class ThreadRun2 implements Runnable {
	
	public void run() {
		for (int i = 0;i <= 5; i++) {
		try {
			System.out.println("Thread-2");
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
  }
}

public class TwoThreadsRunnable {
	public static void main(String[] args) {
		
		ThreadRun1 tOne = new ThreadRun1();
		ThreadRun2 tTwo = new ThreadRun2();
		
		Thread t1 = new Thread(tOne);
		Thread t2 = new Thread(tTwo);
		t1.start();
		t2.start();
	}
}
