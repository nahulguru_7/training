/*8.Addition,Substraction,Multiplication and Division concepts are achieved 
using Lambda expression and functional interface.

------------------------------WBS------------------------------------------
1.Requirements:
   - Lambda with single method interface 
2.Entities
   - LambdaExpDemo
   - Arthmetic (Interface)
3.Function Declaration
    - int operation(int a, int b)
4.Jobs to be done
    1.Create a interface as Arithmetic and operation(int a, int b) method.
    2.Create Lambda initialising the Addition,Substraction,Multiplication and Division the two variables. 
    3.Print the each Arithmetic operations.
    
Psudeo Code:
''''''''''''
interface Arithmetic {
	int operation(int a, int b);
}

public class LambdaExpression {
	public static void main(String[] args) {
		Arithmetic addition = (int a, int b) -> (a + b); 
		System.out.println("Addition = " + addition.operation(11, 16));
		Arithmetic subtraction = (int a, int b) -> (a - b);
		System.out.println("Subtraction = " + subtraction.operation(6, 3));
		Arithmetic multiplication = (int a, int b) -> (a * b);
		System.out.println("Multiplication = " + multiplication.operation(12, 6));
		Arithmetic division = (int a, int b) -> (a / b);
		System.out.println("Division = " + division.operation(22, 11));
	}
}
---------------------------------Program---------------------------------*/

interface Arithmetic {
	int operation(int a, int b);
}

public class LambdaExpression {
	public static void main(String[] args) {

		Arithmetic addition = (int a, int b) -> (a + b); //Adding two elements
		System.out.println("Addition = " + addition.operation(11, 16));
		Arithmetic subtraction = (int a, int b) -> (a - b);//Substract two elements
		System.out.println("Subtraction = " + subtraction.operation(6, 3));
		Arithmetic multiplication = (int a, int b) -> (a * b);//Multiply two elements
		System.out.println("Multiplication = " + multiplication.operation(12, 6));
		Arithmetic division = (int a, int b) -> (a / b);//Dividing two elements
		System.out.println("Division = " + division.operation(22, 11));
	}
}
