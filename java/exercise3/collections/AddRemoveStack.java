/*5.Add and remove the elements in stack.
---------------------------------------WBS----------------------------------
1.Requirements:
   - Stack with elements
2.Entities
    - AddRemoveStack
3.Function Declaration
    - public static void main(String [] args)
4.Jobs to be done
     1.Create a stack cars using generic type
     2.Add 4 values in the stack using push method.
     3.Using pop method removing stack top element
     4.Print the stack elements.

Psudeo Code:
''''''''''''
public class AddRemoveStack {
    public static void main(String [] args) {
        Stack<String> cars = new Stack<String>();
        ADD 5 ELEMENTS
        REMOVE AN ELEMENT
        System.out.println(cars); 
    }
}

------------------------------------Program---------------------------------- 
 */
import java.util.Stack;
public class AddRemoveStack {
    public static void main(String [] args) {
        Stack<String> cars = new Stack<String>();  //Creating Stack using generic type
        cars.push("Honda"); //Adding a element to stack
        cars.push("Tesla");
        cars.push("Bajaj");
        cars.push("TATA"); 
        cars.pop();  //Removing a element to stack
        System.out.println(cars); //Printing the stack elements
    }
}
