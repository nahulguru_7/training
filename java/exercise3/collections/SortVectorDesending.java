/* 3.Code for sorting vetor list in descending order.
------------------------------WBS------------------------------------------
1.Requirements:
   - Vector with elements.
2.Entities
   - SortVectorDesending.
3.Function Declaration
   - public static void main (String [] args)
4.Jobs to be done
    1.Create a vector as Furnitures. 
    2.Add 6 values in the vector.
    3.Print the vector elements.
    4.Reversing vetor elements using reverseOrder method. 
    5.Print the reversed vector.
    
Psudeo Code:
''''''''''''
public class SortVectorDesending {
	public static void main (String [] args) {
		Vector<String> 	Furnitures = new Vector<String>();
		ADD 5 ELEMENTS
		System.out.println(Furnitures); 
		Collections.sort(Furnitures, Collections.reverseOrder()); 		 
		System.out.println(Furnitures);
	}
}

------------------------------Program------------------------------------------
 */
import java.util.Collections;
import java.util.Vector;

public class SortVectorDesending {
	public static void main (String [] args) {
		Vector<String> 	Furnitures = new Vector<String>();
		Furnitures.add("chair");
		Furnitures.add("bench");
		Furnitures.add("desk");
		Furnitures.add("folding chair");
		Furnitures.add("mirror");
		Furnitures.add("patio table");
		System.out.println(Furnitures); //Printing Vector elements
		Collections.sort(Furnitures, Collections.reverseOrder()); //Reversing Vector elements		 
		System.out.println(Furnitures);
	}
}
