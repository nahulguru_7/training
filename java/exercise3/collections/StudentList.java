/*6.LIST CONTAINS 10 STUDENT NAMES
    krishnan, abishek, arun,vignesh, kiruthiga, murugan,
    adhithya,balaji,vicky, priya and display only names starting with 'A'.
------------------------------WBS------------------------------------------
Requirements:
   - ArrayList with values
Entities
   - StudentList
Function Declaration
   - public static void main (String [] args)
Jobs to be done
    1.Create a array list as students.
    2.Add 10 values in the array list.
    3.Display only names starting with 'A'.
    
Psudeo Code:
''''''''''''
public class StudentList {
	public static void main(String[] args) {
	List<String> students = new ArrayList<String>();
	ADD 10 ELEMENTS TO LIST 
	for(String student : students) {
	    if(student.startsWith("a")) System.out.println(student);
	    }
    }
 }

------------------------------Program------------------------------------------   
    */
import java.util.ArrayList;
import java.util.List;

public class StudentList {
	public static void main(String[] args) {
	List<String> students = new ArrayList<String>();
	students.add("krishnan"); 	//Add 10 values in the list
	students.add("abishek");
	students.add("arun");
	students.add("vignesh");
	students.add("kiruthiga");
	students.add("murugan");
	students.add("adhithya");
	students.add("balaji");
	students.add("vicky");
	students.add("priya");
	for(String student : students) {
	    if(student.startsWith("a")) System.out.println(student);
	    }
    }
 }

