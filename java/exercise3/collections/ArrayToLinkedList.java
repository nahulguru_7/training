/* 1.Create an array list with 7 elements, and create an empty linked 
list add all elements of the array list to linked list ,traverse the elements and display the result
---------------------------------------WBS----------------------------------
1.Requirements:
    - ArrayList with elements.
2.Entities
    - ArrayToLinkedList
3.Function Declaration
    - public static void main(String [] args)
4.Jobs to be done
    1.Create a array list as cars.
    2.Add 7 values in the ArrayList.
    3.Create linked list name car.
    4.Add all values from array list to linked list.
    5.Using iterator print all values.
    
Psudeo Code:
''''''''''''
public class ArrayToLinkedList {
	public static void main(String [] args) {
		List<String> cars = new ArrayList<String>();
		ADD 7 ELEMENTS
		System.out.println("Elements in the array List : "+cars);
		List<String> car = new LinkedList<String>(); 
        while(car.size() > 0) {
            cars.add(car.remove(0));
            }
		System.out.println("Elements in the Linked List : "+car);
        Iterator<String> iterator = car.listIterator(4); 
        System.out.println("After Traverse the elements using Iterator:"); 
        while(iterator.hasNext()){ 
           System.out.println(iterator.next()); 
        } 
	}
}
---------------------------------------Program----------------------------------
*/

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ArrayToLinkedList {
	public static void main(String [] args) {
		List<String> cars = new ArrayList<String>();
		cars.add("BMW"); 	//Add 7 values in the list
		cars.add("Mazda");
		cars.add("Lexus");
		cars.add("Audi");
		cars.add("Mecleran");
		cars.add("RR");
		cars.add("Dodge");
		System.out.println("Elements in the array List : "+cars);
		List<String> car = new LinkedList<String>(); //Creating LinkedList
        while(car.size() > 0) {
            cars.add(car.remove(0)); //Removing Arraylist elements adding to LinkedList
            }
		System.out.println("Elements in the Linked List : "+car);
        Iterator<String> iterator = car.listIterator(4); 
        System.out.println("After Traverse the elements using Iterator:"); 
        while(iterator.hasNext()){ //Printing the linked list elements 
           System.out.println(iterator.next()); 
        } 
	}
}
