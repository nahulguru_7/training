/*8 Districts are shown below
    Madurai, Coimbatore, Theni, Chennai, Karur, Salem, Erode, Trichy  
     to be converted to UPPERCASE.
---------------------------------------WBS----------------------------------
1.Requirements:
   * List with values
2.Entities
   * ConvertIntoUpperCase
3.Function Declaration
   -none-
4.Jobs to be done
    1.Create a list as city.
    2.Add 8 values in the list.
    3.Converted array list to UPPERCASE using toUpperCase method.
    4.Print the array list.

Psudeo Code:
''''''''''''
public class ConvertIntoUpperCase {
	public static void main(String[] args) {
	List<String> bikes = new ArrayList<String>();
	Add 8 ELEMENTS IN LISTS
	for(String bike : bikes) {
	     System.out.println(bike.toUpperCase() ); //Converting the bikes name to UpperCase
	    }
    }
}

---------------------------------------Program----------------------------------
     */
import java.util.ArrayList;
import java.util.List;

public class ConvertIntoUpperCase {
	public static void main(String[] args) {
	List<String> bikes = new ArrayList<String>();
	bikes.add("royal Enfield"); 	//Add 8 values in the list
	bikes.add("honda");
	bikes.add("tVS ");
	bikes.add("rX100");
	bikes.add("rX150");
	bikes.add("fZ");
	bikes.add("r15");
	for(String bike : bikes) {
	     System.out.println(bike.toUpperCase() ); //Converting the bikes name to UpperCase
	    }
    }
}
