import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileReaderWriter {
	public static void main(String[] args) throws IOException {
		String readerPath = "C:/Users/santh/eclipse-workspace/JavaEE-Demo/io.stream/Content";
		@SuppressWarnings("resource")
		FileReader reader = new FileReader(readerPath);
		String writerPath = "C:/Users/santh/eclipse-workspace/JavaEE-Demo/io.stream/AnotherContent";
		FileWriter writer = new FileWriter(writerPath);
		
		int charRead = -1;
		while ((charRead = reader.read()) != -1) {
			writer.write((char)charRead);
		}
		System.err.println("File Successfully Copied");
		writer.close();
	}
}
