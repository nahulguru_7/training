
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileOutputStreams {
    public static void main(String[] args) {
        try {
            File inputFile = new File("C:/Users/santh/eclipse-workspace/JavaEE-Demo/io.stream/Content");
            File outputFile = new File("C:/Users/santh/eclipse-workspace/JavaEE-Demo/io.stream/AnotherContent");

            FileInputStream readFile = new FileInputStream(inputFile);
            FileOutputStream writeFile = new FileOutputStream(outputFile);
            String text = "This Content writtern by using Writer";
            int c;

            while ((c = readFile.read()) != -1) {
               writeFile.write(c);
            }
            System.out.println("Successfully Copied");
            readFile.close();
            writeFile.close();
        } catch (FileNotFoundException e) {
            System.err.println("FileStreamsTest: " + e);
        } catch (IOException e) {
            System.err.println("FileStreamsTest: " + e);
        }
    }
}