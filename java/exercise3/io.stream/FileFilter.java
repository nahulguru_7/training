
import java.io.File;
import java.io.FilenameFilter;
 
public class FileFilter {
 
    public static void main(String[] args) {
        
    	String PATH = "C:/Users/santh/eclipse-workspace/JavaEE-Demo/io.stream/FileFilter";
    	
        File myFile = new File(PATH);
         
        if(!myFile.exists()) {
            System.out.println("The file does not exist...");
            System.exit(1);
        }
        FilenameFilter filter = new MyFileFilterClass();
         
        // a list that contains only files with the specified extension
        File[] fileList = myFile.listFiles(filter);
         
        if(fileList.length > 0) {
            System.out.println("Containing files with the use of filter are:");
             
            for(File file : fileList) {
                System.out.println(file.getAbsolutePath());
            }
        } else {
            System.out.println("There are not such files into the "+myFile);
        }
         
        System.out.println("---------------------------------------------------");
         
        // list without a file filter
        File[] listNoExt = myFile.listFiles();
         
        if(fileList.length > 0) {
            System.out.println("Containing files without the filter are:");
             
            for(File file : listNoExt) {
                System.out.println(file.getAbsolutePath());
            }
        } else {
            System.out.println("There are no files at all");
        }
 
    }
 
}