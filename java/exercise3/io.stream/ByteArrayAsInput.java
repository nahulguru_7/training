
import java.io.ByteArrayInputStream;
import java.io.IOException;  
public class ByteArrayAsInput {  
  public static void main(String[] args) throws IOException {  
    byte[] buf = { 5, 1, 2, 3 };  
    // Create the new byte array input stream  
    ByteArrayInputStream byt = new ByteArrayInputStream(buf);  
    int numbers;  
    while ((numbers = byt.read()) != -1) {  
      //Conversion of a byte into character   
      System.out.println(numbers);
    }  
  }  
} 