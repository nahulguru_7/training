import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class ByteArrayOutputStreamDemo {
    public static void main(String[] args) throws IOException {
        FileOutputStream fileOutputStream1 = null;
        FileOutputStream fileOutputStream2 = null;
        ByteArrayOutputStream byteArrayOutputStream = null;
        try {
            fileOutputStream1 = new FileOutputStream("C:/Users/santh/eclipse-workspace/JavaEE-Demo/io.stream/sample1.txt");
            fileOutputStream2 = new FileOutputStream("C:/Users/santh/eclipse-workspace/JavaEE-Demo/io.stream/sample2.txt");
            byteArrayOutputStream = new ByteArrayOutputStream();

            String str = "Sample File Created";
            byte[] byteArray = str.getBytes();
            byteArrayOutputStream.write(byteArray);

            /*
             * Writes the complete contents of this byte
             * array output stream to the specified output
             * stream argument
             */
            byteArrayOutputStream.writeTo(fileOutputStream1);
            byteArrayOutputStream.writeTo(fileOutputStream2);

            byteArrayOutputStream.flush();
            System.out.println("Successfully written to two files...");
        }
        finally {
            if (fileOutputStream1 != null)
            {
                fileOutputStream1.close();
            }
            if (fileOutputStream2 != null)
            {
                fileOutputStream2.close();
            }
            if (byteArrayOutputStream != null)
            {
                byteArrayOutputStream.close();
            }
        }
    }
}