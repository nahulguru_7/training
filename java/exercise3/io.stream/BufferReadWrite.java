
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;  

public class BufferReadWrite {  
public static void main(String[] args) throws Exception {   
	String writerPath = "C:/Users/santh/eclipse-workspace/JavaEE-Demo/io.stream/BufferFile"; 
    BufferedWriter bufferWriter = new BufferedWriter(new FileWriter(writerPath));  
    bufferWriter.write("Welcome to BufferWriter file");  
    bufferWriter.close();  
    System.out.println("Successfully Writtern");
    
    File file = new File("C:/Users/santh/eclipse-workspace/JavaEE-Demo/io.stream/BufferFile"); 
    
    @SuppressWarnings("resource")
	BufferedReader bufferReader = new BufferedReader(new FileReader(file)); 
    
    String reader; 
    while ((reader = bufferReader.readLine()) != null) 
      System.out.println(reader); 
    } 
    
}