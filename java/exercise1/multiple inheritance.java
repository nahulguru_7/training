interface one 
{  
	default void show() 
	{ 
		System.out.println("Default one"); 
	} 
} 

interface two 
{ 
	default void show() 
	{ 
		System.out.println("Default two"); 
	} 
} 
class TestClass implements one, two 
{ 
	public void show() 
	{  
		one.super.show(); 
		tw0.super.show(); 
	} 

	public static void main(String args[]) 
	{ 
		TestClass d = new TestClass(); 
		d.show(); 
	} 
} 
