package Maths;

public class BasicMath{

    float a, b;
	
	public BasicMath(float num1, float num2){
		a = num1;
		b = num2;
	}
	
	public BasicMath(){
		a = 0.00f;
		b = 0.00f;
	}
	
	public float add(){
	    return a+b;
	}
	
	public float sub(){
	    return a-b;
	}
	
	public float mul(){
	    return a*b;
	}
	
	public float div(){
	    return a/b;
	}
}